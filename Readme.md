# Guidelines

Below are some basic guidelines for our development



## Basics
1.  No CSS resets
1.  Be semantic - call things what they are. Name your component what the filename is, and give it a class that matches.
1.  In Vue templates, use components like `<component-name/>` not like `<ComponentName>`.
2.  Component names should prefer full words over abbreviations like `UserProfileOptions.vue` not `UsrProfOpts`
3.  Avoid use of element name selectors in CSS. Don't use `h2`, give it a `.title` class instead.
4.  In general avoid use of CSS pseudo elements like `:content`, `:before`, `:after`. Except for list bullets or underlines.
5.  No use of floats
6.  Don’t use `#` ID’s unless using for JS selectors or actually need page anchors.
7.  Use a central z-index location for major structural components, increment by 100’s
8.  For component level z-index set top level to 0 and increment by 10
9.  Use positioning sparingly. In general most people over use `position:absolute`.
10. If using useless markup to get a desired style, you’re doing it wrong. For example wrapping divs, centering divs.
11. Define CSS for active and hover states at the end of component, followed by breakpoints at end of file please!
12. When using SCSS if you’re going more than 2 levels deep, question yourself. Think of the top level element class as a namespace, so things in it don’t need namespaces too.
13. For class names use is-{state} and has-{feature} or not-{type}. Like is-active, is-opened, has-video, not-case-study, is-active.
14. Common element class names: block, grid, panel, menu, overlay, meta, title, section, section-title.
15. Margin top/bottom, padding left/right.
16. Use lodash ensure to only import the functions you use. Don't try to use map() and filter() in inventive ways.
17. Don’t fight the browser - scroll, events, URLs etc…
18. Use white spacing in your templates.
19. Order your CSS in the same order your markup is in. Top to bottom as coded.
20. A `switch` statement is better than a lot of if-else conditions
21. Don't use icon fonts, use SVGs. SVGO is a good tool for optimizing SVGs.
22. If your component accesses $store or $route directly, you're doing it wrong. Use props and events instead.

## Important Concepts
1.  We care about Chrome, Safari and Firefox in that order.
1.  You can write breakpoints without needing a media query generally. Often using width and max-width is enough. Good break points will really just be font-size and reducing columns perhaps.
1.  Understand collapsible margins!
1.  Understand intrinsic ratio sizing!
2.  Understand when to use v-html, v-text
3.  Clean up after yourself, don’t leave behind old code!
4.  Really think hard about what can be a component. Makes a site so hard if you don’t use components.
5.  Don’t do things rough with the expectation of coming back to it. Do it right the first time, think it through.
6.  Learn how to spot red flags. Long file? Deep nested CSS? Watching a lot of things? Lots of specific break points? Committing to the store a lot?


## Good reading:
1.  [Vue Style Guide](https://v3.vuejs.org/style-guide/)
2.  [The introduction to Reactive Programming you've been missing](https://gist.github.com/staltz/868e7e9bc2a7b8c1f754)
3.  [Good overview of ES6 destructuring](https://2ality.com/2015/01/es6-destructuring.html)
4.  [A good book on how to write good CSS](https://maintainablecss.com/)
5.  [Use console.log() like a pro](https://markodenic.com/use-console-log-like-a-pro/)


## Useful FrontEnd tools:
1.  [Demystifing Nth-Child in CSS](http://www.nealgrosskopf.com/tech/resources/80/)
1.  [Flexy Boxes Playground](https://the-echoplex.net/flexyboxes/)
1.  [CSS Grid Generator](https://cssgrid-generator.netlify.com/)
2.  [Clippy](https://bennettfeely.com/clippy/)
